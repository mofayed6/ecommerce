<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */



Auth::routes();

Route::get('email/verify/{token}', 'Auth\RegisterController@verify');

Route::group(['prefix' => 'admin'], function () {

    Route::get('login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('login', ['as'=>'admin-login','uses'=>'Auth\AdminLoginController@login']);

    Route::group(['middleware' => 'admin:admin'], function () {

        Route::get('dashboard', 'Admin\HomeController@index')->name('admin.dashboard');
        Route::post('logout', ['as'=>'admin.logout','uses'=>'Auth\AdminLoginController@logout']);

        //languages group
        Route::get('languages/create', ['as'=>'language.create','uses'=>'Admin\LanguageController@create']);
        Route::post('languages/store', ['as'=>'language.store','uses'=>'Admin\LanguageController@store']);
        Route::get('languages/edit/{id}', ['as'=>'language.edit','uses'=>'Admin\LanguageController@edit']);
        Route::post('languages/update/{id}', ['as'=>'language.update','uses'=>'Admin\LanguageController@update']);
        Route::get('languages/delete/{id}', ['as'=>'language.delete','uses'=>'Admin\LanguageController@destroy']);
        Route::get('languages','Admin\LanguageController@index')->name('languages');

        //users group
        Route::get('users/create', ['as'=>'user.create','uses'=>'Admin\UsersController@create']);
        Route::post('users/store', ['as'=>'user.store','uses'=>'Admin\UsersController@store']);
        Route::get('users/edit/{id}', ['as'=>'user.edit','uses'=>'Admin\UsersController@edit']);
        Route::post('users/update/{id}', ['as'=>'user.update','uses'=>'Admin\UsersController@update']);
        Route::get('users/delete/{id}', ['as'=>'user.delete','uses'=>'Admin\UsersController@destroy']);
        Route::get('users','Admin\UsersController@index')->name('users');

        Route::get('profile/{id}', 'Admin\ProfileController');

        //product group
        Route::get('products/create', ['as'=>'product.create','uses'=>'Admin\ProductController@create']);
        Route::post('products/store', ['as'=>'product.store','uses'=>'Admin\ProductController@store']);
        Route::get('products/edit/{id}', ['as'=>'product.edit','uses'=>'Admin\ProductController@edit']);
        Route::post('products/update/{id}', ['as'=>'product.update','uses'=>'Admin\ProductController@update']);
        Route::get('products/delete/{id}', ['as'=>'product.delete','uses'=>'Admin\ProductController@destroy']);
        Route::get('products','Admin\ProductController@index')->name('products');


        //admin group
        Route::get('admins/create', ['as'=>'admin.create','uses'=>'Admin\AdminsController@create']);
        Route::post('admins/store', ['as'=>'admin.store','uses'=>'Admin\AdminsController@store']);
        Route::get('admins/edit/{id}', ['as'=>'admin.edit','uses'=>'Admin\AdminsController@edit']);
        Route::post('admins/update/{id}', ['as'=>'admin.update','uses'=>'Admin\AdminsController@update']);
        Route::get('admins/delete/{id}', ['as'=>'admin.delete','uses'=>'Admin\AdminsController@destroy']);
        Route::get('admins','Admin\AdminsController@index')->name('admins');

    });
});


Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {

        Route::get('/', 'Site\HomeController@index')->name('home')->middleware('throttle:30,1');

    });

