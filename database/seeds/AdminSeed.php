<?php

use Illuminate\Database\Seeder;

class AdminSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lang_1 = \App\Language::find(1);
        \App\Admin::create([
            'id' => 1,
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'language_id' => $lang_1->id,
            'password' => \Illuminate\Support\Facades\Hash::make('123456'),
        ]);

    }
}
