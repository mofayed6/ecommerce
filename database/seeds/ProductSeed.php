<?php

use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile;

class ProductSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lang_1 = \App\Language::find(1);

        for ($i = 1; $i <= 5; $i++) {
            $product = \App\Product::create([
                'name' => "Product . $i ",
                'description' => "description' . $i " ,
                'language_id' => $lang_1->id,
                'price' => 1000,
                'img' => UploadedFile::fake()->image("picture_{$i}.jpg", 400, 400)->store('products', 'public'),
            ]);
        }


        $lang_2 = \App\Language::find(2);
        for ($i = 6; $i <= 10; $i++) {
            $product = \App\Product::create([
                'name' => "Product . $i ",
                'description' => "description' . $i ",
                'language_id' => $lang_2->id,
                'price' => $i * 20,
                'img' => UploadedFile::fake()->image("picture_{$i}.jpg", 400, 400)->store('products', 'public'),
            ]);

        }

    }
}
