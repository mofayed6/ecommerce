<?php

use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile;

class LanguageSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Language::create([
            'id' => 1,
            'slogan' => 'Ar',
            'title' => 'العربية',
            'image' => UploadedFile::fake()->image("picture_1.jpg", 400, 400)->store('languages', 'public'),
        ]);


        \App\Language::create([
            'id' => 2,
            'slogan' => 'En',
            'title' => 'English',
            'image' => UploadedFile::fake()->image("picture_2.jpg", 400, 400)->store('languages', 'public'),
        ]);
    }
}

