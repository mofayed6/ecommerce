<?php

/**
 * function get title language form database when local == lang
 * @param $langLocale
 * @return mixed
 */
function lang($langLocale){
    $languages = \App\Language::get();

    foreach ($languages as $lang){
        if(strtolower($lang->slogan) == $langLocale || strtolower($lang->title) == $langLocale){
            return $lang->title;
        }
    }
}
