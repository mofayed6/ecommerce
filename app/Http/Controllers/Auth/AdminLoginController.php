<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;


class AdminLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $guard = 'admin';

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }


    public function showLoginForm()
    {
        return view('auth.adminLogin');
    }

    public function login(Request $request)
    {

        $request->validate([

            'password' => 'required|min:5',

            'email' => 'required|email'

        ], [

            'email.required' => 'Email is required',

            'password.required' => 'Password is required'

        ]);

        if(auth()->guard('admin')->attempt($request->only(['email','password']),$request->remember)){
                return redirect()->intended('/admin/dashboard');
        }

        return redirect()->back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => 'Incorrect email address or password',
            ]);
    }

    public function logout()
    {
        //dd('ahmed');
        if (auth()->guard('admin')->check()) {

            auth()->guard('admin')->logout();
        }
        return redirect(url('/admin/login'));
    }
}
