<?php

namespace App\Http\Controllers\Site;

use App\Language;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class HomeController extends Controller
{
    /**
     * function get products and pass count to service provider
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $languaes = Language::where('title',app()->getLocale())->orWhere('slogan',app()->getLocale())->first();
        $products = Product::where('language_id',$languaes->id)->get();
        $count = Product::where('language_id',$languaes->id)->get()->count();

        return view('users.home',compact('products','count'));
    }
}
