<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{

    /**
     * Show the profile for the given admin.
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke($id)
    {
       // dd($id);
        $admins = Admin::find($id);
      //  dd($admins);

        return view('backend.admins.profile',compact('admins'));

    }
}
