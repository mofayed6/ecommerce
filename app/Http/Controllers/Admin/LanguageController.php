<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\LanguagesDataTable;
use App\Http\Requests\Languages;
use App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
class LanguageController extends Controller
{
    /**
     * show all language
     * @param LanguagesDataTable $dataTable
     * @return mixed
     */
    public function index(LanguagesDataTable $dataTable)
    {
        return $dataTable->render('backend.language.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
     {

         return view('backend.language.create');
     }

    /**
     * Store a newly created resource in storage.
     * @param Languages $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(Languages $request)
     {

         $new_language = Language::create([
             'title' => $request->title,
             'slogan' => $request->slogan,
             'image'=> ($request->hasFile('image')) ? $request->image->store('languages') : 'lang.png',
         ]);


         if ($new_language) {
             if($request->ajax()){
                 return response()->json([
                     'requestStatus' => true,
                     'message' => 'added successfully']);
             }

             return redirect()->back()->with('success', 'added successfully');
         }

         if($request->ajax()){
             return response()->json([
                 'requestStatus' => true,
                 'message' => 'add error']);
         }


         return redirect()->back()->with('error', 'add error');

     }

    /**
     * Show the form for editing a resource.
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
     public function edit($id)
     {

         $languages = Language::find($id);
         return view('backend.language.edit',compact('languages'));
     }

    /**
     * update a admin resource in storage.
     * @param Languages $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
     public function update(Languages $request ,$id)
     {
         $languages = Language::find($id);

         $new_language = $languages->update([
             'title' => $request->title,
             'slogan' => $request->slogan,
             'image'=>  ($request->hasFile('image')) ? $request->image->store('languages') : $languages->image

         ]);



         if ($new_language) {
             if($request->ajax()){
                 return response()->json([
                     'requestStatus' => true,
                     'message' => 'added successfully']);
             }

             return redirect()->back()->with('success', 'added successfully');
         }

         if($request->ajax()){
             return response()->json([
                 'requestStatus' => true,
                 'message' => 'add error']);
         }


         return redirect()->back()->with('error', 'add error');
     }

    /**
     * delete item when send id
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {

        $languages = Language::find($id);

        $check = $languages->delete();


        if ($check) {
            return Response::json($id, '200');
        } else {
            return redirect()->back()->with('error', 'delete error');

        }
    }
}
