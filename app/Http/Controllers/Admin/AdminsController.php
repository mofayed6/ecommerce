<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\DataTables\AdminsDataTable;
use App\Http\Requests\Admins;
use App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;

class AdminsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AdminsDataTable $dataTable)
    {
        return $dataTable->render('backend.admins.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = Language::pluck('title','id');
        return view('backend.admins.create',compact('languages'));
    }


    /**
     * Store a newly created resource in storage.
     * @param Admins $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(Admins $request)
    {

        $new_admins = Admin::create([
            'name' => $request->name,
            'language_id' => $request->language_id,
            'email'=>$request->email,
            'password'=>encrypt($request->password),
        ]);


        if ($new_admins) {
            if($request->ajax()){
                return response()->json([
                    'requestStatus' => true,
                    'message' => 'added successfully']);
            }

            return redirect()->back()->with('success', 'added successfully');
        }

        if($request->ajax()){
            return response()->json([
                'requestStatus' => true,
                'message' => 'add error']);
        }


        return redirect()->back()->with('error', 'add error');

    }


    /**
     * Show the form for editing a resource.
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $languages = Language::pluck('title','id');
        $admins = Admin::find($id);
        return view('backend.admins.edit',compact('admins','languages'));
    }

    /**
     * update a admin resource in storage.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request , $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);


        $admins = Admin::find($id);

        $new_admins= $admins->update([
            'name' => $request->name,
            'language_id' => $request->language_id,
            'email'=>$request->email,
            'password'=> ($request->has('password')) ? encrypt($request->password) : $admins->password,
        ]);


        if ($new_admins) {
            if($request->ajax()){
                return response()->json([
                    'requestStatus' => true,
                    'message' => 'added successfully']);
            }

            return redirect()->back()->with('success', 'added successfully');
        }

        if($request->ajax()){
            return response()->json([
                'requestStatus' => true,
                'message' => 'add error']);
        }


        return redirect()->back()->with('error', 'add error');
    }

    /**
     * delete item when send id
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {

        $admins = Admin::find($id);

        $check = $admins->delete();


        if ($check) {
            return Response::json($id, '200');
        } else {
            return redirect()->back()->with('error', 'delete error');

        }
    }
}
