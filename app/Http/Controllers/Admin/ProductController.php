<?php

namespace App\Http\Controllers\Admin;
use App\DataTables\ProductDataTable;
use App\Http\Requests\Products;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProductDataTable $dataTable)
    {
        return $dataTable->render('backend.products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('backend.products.create');
    }


    /**
     * Store a newly created resource in storage.
     * @param Products $productsRequest
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Products $productsRequest)
    {

        $validated = $productsRequest->validated();

        $new_product= Product::create([
            'name' => $productsRequest->name,
            'language_id' => auth('admin')->user()->language_id,
            'description' => $productsRequest->description,
            'price'=>$productsRequest->price,
            'img'=> $productsRequest->img->store('products')
        ]);

        if ($new_product) {
            if($productsRequest->ajax()){
                return response()->json([
                    'requestStatus' => true,
                    'message' => 'added successfully']);
            }

            return redirect()->back()->with('success', 'added successfully');
        }

        if($productsRequest->ajax()){
            return response()->json([
                'requestStatus' => true,
                'message' => 'add error']);
        }


        return redirect()->back()->with('error', 'add error');

    }

    /**
     * Show the form for editing a resource.
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $products = Product::find($id);
        return view('backend.products.edit',compact('products'));
    }

    /**
     * update a product resource in storage.
     * @param Products $productsRequest
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update(Products $productsRequest ,$id)
    {

        $products = Product::find($id);

        $new_product= $products->update([
            'name' => $productsRequest->name,
            'language_id' => auth('admin')->user()->language_id,
            'description' => $productsRequest->description,
            'price'=>$productsRequest->price,
            'img'=> ($productsRequest->hasFile('img')) ? encrypt($productsRequest->img) : $products->img,
        ]);


        if ($new_product) {
            if($productsRequest->ajax()){
                return response()->json([
                    'requestStatus' => true,
                    'message' => 'added successfully']);
            }

            return redirect()->back()->with('success', 'added successfully');
        }

        if($productsRequest->ajax()){
            return response()->json([
                'requestStatus' => true,
                'message' => 'add error']);
        }


        return redirect()->back()->with('error', 'add error');
    }

    /**
     * delete item when send id
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {

        $products = Product::find($id);

        $check = $products->delete();


        if ($check) {
            return Response::json($id, '200');
        } else {
            return redirect()->back()->with('error', __('containers.delete-error'));

        }
    }
}
