<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\DataTables\UsersDataTable;
use App\Http\Requests\Users;
use App\Language;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UsersDataTable $dataTable)
    {
        return $dataTable->render('backend.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Users $requestUser
     * @return \Illuminate\Http\Response
     */
    public function store(Users $requestUser)
    {
        $validated = $requestUser->validated();

        $new_user = User::create([
            'name' => $requestUser->name,
            'email' => $requestUser->email,
            'phone' => $requestUser->phone,
            'status' => $requestUser->status,
            'password' => Hash::make($requestUser->password),
        ]);


        if ($new_user) {
            if($requestUser->ajax()){
                return response()->json([
                    'requestStatus' => true,
                    'message' => 'added successfully']);
            }

            return redirect()->back()->with('success', 'added successfully');
        }

        if($requestUser->ajax()){
            return response()->json([
                'requestStatus' => true,
                'message' => 'add error']);
        }


        return redirect()->back()->with('error', 'add error');

    }

    /**
     * Show the form for editing a resource.
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $users = User::find($id);
        return view('backend.users.edit', compact('users'));
    }

    /**
     * update a user resource in storage.
     * @param Users $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update(Users $request, $id)
    {

        $users = Admin::find($id);

        $new_user = $users->update([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'status' => $request->status,
            'password' => ($request->has('password')) ? Hash::make($request->password) : $users->password,
        ]);


        if ($new_user) {
            if($request->ajax()){
                return response()->json([
                    'requestStatus' => true,
                    'message' => 'added successfully']);
            }

            return redirect()->back()->with('success', 'added successfully');
        }

        if($request->ajax()){
            return response()->json([
                'requestStatus' => true,
                'message' => 'add error']);
        }


        return redirect()->back()->with('error', 'add error');
    }


    /**
     * delete item when send id
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {

        $users = User::find($id);

        $check = $users->delete();


        if ($check) {
            return Response::json($id, '200');
        } else {
            return redirect()->back()->with('error', 'delete error');

        }
    }
}
