<?php

namespace App\DataTables;

use App\Product;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;

class ProductDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', 'backend.products.action')
            ->editColumn('img', function(Product $product) {
                return '<img src="'.asset("/uploads/".$product->img).'" width="100px" height="100px">';
            })
            ->addIndexColumn()
            ->rawColumns(['action','img']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Language $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Product $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addColumnBefore([
                'defaultContent' => '',
                'data'           => 'DT_Row_Index',
                'name'           => 'id',
                'title'          => '#',
                'render'         => null,
                'orderable'      => true,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ])
            ->minifiedAjax()
            ->parameters([
                'dom'     => 'Blfrtip',
                'order'   => [[0, 'desc']],
                "lengthMenu" => [[10, 25, 50, -1], [10, 25, 50, "All"]],
                'buttons' => [
                    ['extend' =>'create' , 'text' =>  '<i class="fa fa-plus"></i>create' , 'className' =>'dt-button buttons-copy buttons-html5 btn btn-default legitRipple'] ,
                    ['extend' => 'export', 'text' => '<i class="fa fa-download"></i>export', 'className' =>'dt-button buttons-copy buttons-html5 btn btn-default legitRipple'],
                ],
                'responsive'=>true,

            ]);
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $cols =  [
            'name' => ['name' => 'name' ,'data' => 'name' ,'title' => 'name'],
            'img' => ['name' => 'img' ,'data' => 'img' ,'title' => 'image'],
            'price' => ['name' => 'price' ,'data' => 'price' ,'title' => 'price'],
        ];
        $cols['action'] = [ 'exportable' => false, 'printable'  => false, 'searchable' => false, 'orderable'  => false, 'title' => 'action'];

        return $cols;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Product_' . date('YmdHis');
    }
}
