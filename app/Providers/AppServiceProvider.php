<?php

namespace App\Providers;

use App\Language;
use App\Product;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * create directive product return count product with language
         * @param $environment  product count
         */
        Blade::directive('count_product', function ($environment) {
            return $environment;
        });
    }
}
