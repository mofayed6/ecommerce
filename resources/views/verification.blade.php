@extends('users.layouts.app')
@section('title', 'Confirmed')
@section('content')

    <br><br><br><br><br><br><br>
    <section class="section-content bg padding-y">
        <div class="container">
            <div class="col-md-6 mx-auto">
                <div class="card">
                    <header class="card-header">
                        <h4 class="card-title mt-2">Registration </h4>
                    </header>
                    <article class="card-body">
                        You have successfully registered. An email is sent to you for verification.
                    </article>
                </div>
            </div>
        </div>
    </section>
@stop