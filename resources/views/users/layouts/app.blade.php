<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title') - {{ config('app.name') }}</title>
    @include('users.partials.styles')
</head>
<body class="animsition">
@include('users.partials.header')
@yield('content')
@include('users.partials.footer')
@include('users.partials.scripts')
</body>
</html>