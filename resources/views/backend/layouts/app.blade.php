@section('header')
    @include('backend.particals.header')
@show


@section('navbar')
    @include('backend.particals.navbar')
@show



<!-- Page content -->
<div class="page-content">

    <!-- Main sidebar -->
@section('sidebar')
    @include('backend.particals.sidebar')
@show
<!-- /main sidebar -->


    <!-- Main content -->
    <div class="content-wrapper">

        @yield('content')


@yield('script2')

@section('footer')
    @include('backend.particals.footer')
@show








