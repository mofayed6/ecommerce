@if($status == 1)
<span class="badge badge-success">{{ __('users.active') }}</span>
@else
<span class="badge badge-danger">{{ __('users.not-active') }} </span>
@endif