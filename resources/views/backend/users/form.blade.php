@if(isset($users))

    {!! Form::model($users,['route'=>['user.update',$users->id],'method'=>'post','enctype'=>'multipart/form-data','class'=>'form-horizontal ajax','role'=>'form']) !!}
@else
    {!! Form::open(['route'=>'user.store' ,'enctype'=>'multipart/form-data','class'=>'form-horizontal ajax create']) !!}

@endif

<fieldset class="mb-3">

    <div class="form-group row">
        <label class="col-form-label col-lg-2">Name</label>
        <div class="col-lg-10">
            <input type="text" name="name" class="form-control form-data-input" value="{{ isset($users) ? $users->name :  old('name') }}">
            <div class="error validation-error-label text-danger">{{ $errors->first('name') }}</div>
        </div>
    </div>



    <div class="form-group row">
        <label class="col-form-label col-lg-2">Email</label>
        <div class="col-lg-10">
            <input type="email" name="email" class="form-control form-data-input" value="{{ isset($users) ? $users->email :  old('email') }}">
            <div class="error validation-error-label text-danger">{{ $errors->first('email') }}</div>

        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-lg-2">Phone</label>
        <div class="col-lg-10">
            <input type="tel" name="phone" class="form-control form-data-input" value="{{ isset($users) ? $users->phone :  old('phone') }}">
            <div class="error validation-error-label text-danger">{{ $errors->first('phone') }}</div>

        </div>
    </div>


    <div class="form-group row">
        <label class="col-form-label col-lg-2">Password</label>
        <div class="col-lg-10">
            <input type="password" name="password" class="form-control form-data-input">
            <div class="error validation-error-label text-danger">{{ $errors->first('password') }}</div>

        </div>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-lg-2">Status</label>
        <div class="col-lg-10">
            {!! Form::select('status', [1=>'Active',2=>'DisActive'], isset($users) ? $users->status : null,  ['class'=>'form-control form-data-input', 'placeholder' => __('devices.choose-status')]) !!}
            <div class="error validation-error-label text-danger">{{ $errors->first('status') }}</div>

        </div>
    </div>

</fieldset>

<div class="text-right">
    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
</div>


{!! Form::close() !!}