@extends('backend.layouts.app')

@section('content')


    <!-- Content area -->
    <div class="content">

        <!-- Basic datatable -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Products</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>

            {!! $dataTable->table(['class' => 'table table-delete-action datatable-button-html5-columns' , 'width' => '100%'],true) !!}
        </div>
        <!-- /basic datatable -->

        <!-- /column selectors -->


    </div>
    <!-- /content area -->




@endsection



@section('script2')

    <script type="text/javascript" src="{{  asset('/global_assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="{{  asset('/global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js')}}"></script>
    <script type="text/javascript" src="{{  asset('/global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js')}}"></script>
    <script type="text/javascript" src="{{  asset('/global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js')}}"></script>
    <script type="text/javascript" src="{{  asset('/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
    {{--<script type="text/javascript" src="{{  asset('/global_assets/js/pages/datatables_extension_buttons_html5.js')}}"></script>--}}
    <script src="{{ asset('/vendor/datatables/buttons.server-side.js') }}"></script>
    {!! $dataTable->scripts() !!}
@endsection



