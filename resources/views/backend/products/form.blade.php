@if(isset($products))

    {!! Form::model($products,['route'=>['product.update',$products->id],'method'=>'post','enctype'=>'multipart/form-data','class'=>'form-horizontal ajax','role'=>'form']) !!}
@else
    {!! Form::open(['route'=>'product.store' ,'enctype'=>'multipart/form-data','class'=>'form-horizontal ajax create']) !!}

@endif

<fieldset class="mb-3">

    <div class="form-group row">
        <label class="col-form-label col-lg-2">Name</label>
        <div class="col-lg-10">
            <input type="text" name="name" class="form-control form-data-input" value="{{ isset($products) ? $products->name :  old('name') }}">
            <div class="error validation-error-label text-danger">{{ $errors->first('name') }}</div>

        </div>
    </div>



    <div class="form-group row">
        <label class="col-form-label col-lg-2">Description</label>
        <div class="col-lg-10">
            <textarea rows="3" cols="3" name="description" class="form-control form-data-input" >{{ isset($products) ? $products->description :  old('description') }}</textarea>
        </div>
    </div>


    <div class="form-group row">
        <label class="col-form-label col-lg-2">Price</label>
        <div class="col-lg-10">
            <input type="number" name="price" class="form-control form-data-input" value="{{ isset($products) ? $products->price :  old('price') }}">
            <div class="error validation-error-label text-danger">{{ $errors->first('price') }}</div>

        </div>
    </div>


    <div class="form-group row">
        <label class="col-form-label col-lg-2">Image</label>
        <div class="col-lg-10">
            <input type="file" name="img" class="form-control form-data-input">
            <div class="error validation-error-label text-danger">{{ $errors->first('img') }}</div>

        </div>
    </div>



</fieldset>

<div class="text-right">
    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
</div>


{!! Form::close() !!}