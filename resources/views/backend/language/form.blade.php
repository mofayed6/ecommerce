@if(isset($languages))

    {!! Form::model($languages,['route'=>['language.update',$languages->id],'method'=>'post','enctype'=>'multipart/form-data','class'=>'form-horizontal ajax','role'=>'form']) !!}
@else
    {!! Form::open(['route'=>'language.store' ,'enctype'=>'multipart/form-data','class'=>'form-horizontal ajax create']) !!}

@endif


    <fieldset class="mb-3">

        <div class="form-group row">
            <label class="col-form-label col-lg-2">Title</label>
            <div class="col-lg-10">
                <input type="text" name="title" class="form-control form-data-input" value="{{ isset($languages) ? $languages->title :  old('title') }}">
                <div class="error validation-error-label text-danger">{{ $errors->first('title') }}</div>

            </div>
        </div>

        <div class="form-group row">
            <label class="col-form-label col-lg-2">Slogan</label>
            <div class="col-lg-10">
                <input type="text" name="slogan" class="form-control form-data-input" value="{{ isset($languages) ? $languages->slogan :  old('slogan') }}">
                <span>please add slogan like (ar,en,su,fr,en-US)</span>
                <div class="error validation-error-label text-danger">{{ $errors->first('slogan') }}</div>

            </div>
        </div>

        <div class="form-group row">
            <label class="col-form-label col-lg-2">Image</label>
            <div class="col-lg-10">
                <input type="file" name="image" class="form-control form-data-input">
                <div class="error validation-error-label text-danger">{{ $errors->first('image') }}</div>

            </div>
        </div>


    </fieldset>

    <div class="text-right">
        <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
    </div>


{!! Form::close() !!}
