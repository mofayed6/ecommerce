
<td class="text-center">
<div class="list-icons">
    <div class="dropdown">
        <a href="#" class="list-icons-item" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>

        <div class="dropdown-menu dropdown-menu-right">
            <a href="{{ url('admin/admins/edit/'.$id) }}" class="dropdown-item"><i class="icon-pencil5"></i> Edit</a>
            <a class="destroy dropdown-item" id="{{$id}}" data-token="{{ csrf_token() }}" data-route="{{ route('admin.delete', $id) }}"><i class="icon-bin"></i>Delete </a>

        </div>
    </div>
</div>
</td>



