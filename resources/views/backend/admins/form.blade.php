@if(isset($admins))

    {!! Form::model($admins,['route'=>['admin.update',$admins->id],'method'=>'post','enctype'=>'multipart/form-data','class'=>'form-horizontal ajax','role'=>'form']) !!}
@else
    {!! Form::open(['route'=>'admin.store' ,'enctype'=>'multipart/form-data','class'=>'form-horizontal ajax create']) !!}

@endif

<fieldset class="mb-3">

    <div class="form-group row">
        <label class="col-form-label col-lg-2">Name</label>
        <div class="col-lg-10">
            <input type="text" name="name" class="form-control form-data-input" value="{{ isset($admins) ? $admins->name :  old('name') }}">
            <div class="error validation-error-label text-danger">{{ $errors->first('name') }}</div>
        </div>
    </div>



    <div class="form-group row">
        <label class="col-form-label col-lg-2">Email</label>
        <div class="col-lg-10">
            <input type="email" name="email" class="form-control form-data-input" value="{{ isset($admins) ? $admins->email :  old('email') }}">
            <div class="error validation-error-label text-danger">{{ $errors->first('email') }}</div>

        </div>
    </div>


    <div class="form-group row">
        <label class="col-form-label col-lg-2">Password</label>
        <div class="col-lg-10">
            <input type="password" name="password" class="form-control form-data-input">
            <div class="error validation-error-label text-danger">{{ $errors->first('password') }}</div>
        </div>
    </div>


    <div class="form-group row">
        <label class="col-form-label col-lg-2">Language</label>
        <div class="col-lg-10">
            {!! Form::select('language_id', $languages, isset($admins) ? $admins->language_id : null,  ['class'=>'form-control form-data-input', 'placeholder' => __('devices.choose-lang')]) !!}
            <div class="error validation-error-label text-danger">{{ $errors->first('language_id') }}</div>
        </div>
    </div>


</fieldset>

<div class="text-right">
    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
</div>


{!! Form::close() !!}