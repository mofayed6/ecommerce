@component('mail::message')
    # Hello,

    Welcome to Market,
    Please Click the button below to activate your account

@component('mail::button', ['url' => url('email/verify/'.$email_token)])
    Activate Account
@endcomponent

Thanks,<br>
    Market
@endcomponent

